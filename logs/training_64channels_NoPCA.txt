Start Subject: 0
== Start Classifier: GradientBoostingClassifier_0.1
[[2529   11]
 [ 511    9]]
TN: 9, FN: 11, TP: 2529, FP: 511
	Accuracy     : 0.83
	Precision    : 0.83
	Recall       : 1.00
	F1           : 0.91
	Informedness : 0.01
== End Classifier: GradientBoostingClassifier_0.1
== -------------
== Start Classifier: AdaBoostClassifier
[[2470   70]
 [ 467   53]]
TN: 53, FN: 70, TP: 2470, FP: 467
	Accuracy     : 0.82
	Precision    : 0.84
	Recall       : 0.97
	F1           : 0.90
	Informedness : 0.07
== End Classifier: AdaBoostClassifier
== -------------
== Start Classifier: KNeighborsClassifier
[[2335  205]
 [ 489   31]]
TN: 31, FN: 205, TP: 2335, FP: 489
	Accuracy     : 0.77
	Precision    : 0.83
	Recall       : 0.92
	F1           : 0.87
	Informedness : -0.02
== End Classifier: KNeighborsClassifier
== -------------
== Start Classifier: DecisionTreeClassifier
[[2089  451]
 [ 404  116]]
TN: 116, FN: 451, TP: 2089, FP: 404
	Accuracy     : 0.72
	Precision    : 0.84
	Recall       : 0.82
	F1           : 0.83
	Informedness : 0.05
== End Classifier: DecisionTreeClassifier
== -------------
== Start Classifier: RandomForestClassifier
[[2522   18]
 [ 519    1]]
TN: 1, FN: 18, TP: 2522, FP: 519
	Accuracy     : 0.82
	Precision    : 0.83
	Recall       : 0.99
	F1           : 0.90
	Informedness : -0.01
== End Classifier: RandomForestClassifier
== -------------
== Start Classifier: LinearDiscriminantAnalysis_svd_Shrinkage:None
[[2394  146]
 [ 397  123]]
TN: 123, FN: 146, TP: 2394, FP: 397
	Accuracy     : 0.82
	Precision    : 0.86
	Recall       : 0.94
	F1           : 0.90
	Informedness : 0.18
== End Classifier: LinearDiscriminantAnalysis_svd_Shrinkage:None
== -------------
== Start Classifier: QuadraticDiscriminantAnalysis
[[2540    0]
 [ 520    0]]
TN: 0, FN: 0, TP: 2540, FP: 520
	Accuracy     : 0.83
	Precision    : 0.83
	Recall       : 1.00
	F1           : 0.91
	Informedness : 0.00
== End Classifier: QuadraticDiscriminantAnalysis
== -------------
== Start Classifier: LinearSVC
/Users/dannynguyen/opt/anaconda3/lib/python3.7/site-packages/ipykernel_launcher.py:47: RuntimeWarning: invalid value encountered in long_scalars
/Users/dannynguyen/opt/anaconda3/lib/python3.7/site-packages/sklearn/svm/base.py:929: ConvergenceWarning: Liblinear failed to converge, increase the number of iterations.
  "the number of iterations.", ConvergenceWarning)
[[2406  134]
 [ 409  111]]
TN: 111, FN: 134, TP: 2406, FP: 409
	Accuracy     : 0.82
	Precision    : 0.85
	Recall       : 0.95
	F1           : 0.90
	Informedness : 0.16
== End Classifier: LinearSVC
== -------------
== Start Classifier: SVC
[[2425  115]
 [ 429   91]]
TN: 91, FN: 115, TP: 2425, FP: 429
	Accuracy     : 0.82
	Precision    : 0.85
	Recall       : 0.95
	F1           : 0.90
	Informedness : 0.13
== End Classifier: SVC
== -------------
Start Subject: 1
== Start Classifier: GradientBoostingClassifier_0.1
[[2544    6]
 [ 503    7]]
TN: 7, FN: 6, TP: 2544, FP: 503
	Accuracy     : 0.83
	Precision    : 0.83
	Recall       : 1.00
	F1           : 0.91
	Informedness : 0.01
== End Classifier: GradientBoostingClassifier_0.1
== -------------
== Start Classifier: AdaBoostClassifier
[[2509   41]
 [ 478   32]]
TN: 32, FN: 41, TP: 2509, FP: 478
	Accuracy     : 0.83
	Precision    : 0.84
	Recall       : 0.98
	F1           : 0.91
	Informedness : 0.05
== End Classifier: AdaBoostClassifier
== -------------
== Start Classifier: KNeighborsClassifier
[[2353  197]
 [ 477   33]]
TN: 33, FN: 197, TP: 2353, FP: 477
	Accuracy     : 0.78
	Precision    : 0.83
	Recall       : 0.92
	F1           : 0.87
	Informedness : -0.01
== End Classifier: KNeighborsClassifier
== -------------
== Start Classifier: DecisionTreeClassifier
[[2100  450]
 [ 405  105]]
TN: 105, FN: 450, TP: 2100, FP: 405
	Accuracy     : 0.72
	Precision    : 0.84
	Recall       : 0.82
	F1           : 0.83
	Informedness : 0.03
== End Classifier: DecisionTreeClassifier
== -------------
== Start Classifier: RandomForestClassifier
[[2547    3]
 [ 510    0]]
TN: 0, FN: 3, TP: 2547, FP: 510
	Accuracy     : 0.83
	Precision    : 0.83
	Recall       : 1.00
	F1           : 0.91
	Informedness : -0.00
== End Classifier: RandomForestClassifier
== -------------
== Start Classifier: LinearDiscriminantAnalysis_svd_Shrinkage:None
[[2425  125]
 [ 327  183]]
TN: 183, FN: 125, TP: 2425, FP: 327
	Accuracy     : 0.85
	Precision    : 0.88
	Recall       : 0.95
	F1           : 0.91
	Informedness : 0.31
== End Classifier: LinearDiscriminantAnalysis_svd_Shrinkage:None
== -------------
== Start Classifier: QuadraticDiscriminantAnalysis
[[2548    2]
 [ 510    0]]
TN: 0, FN: 2, TP: 2548, FP: 510
	Accuracy     : 0.83
	Precision    : 0.83
	Recall       : 1.00
	F1           : 0.91
	Informedness : -0.00
== End Classifier: QuadraticDiscriminantAnalysis
== -------------
== Start Classifier: LinearSVC
/Users/dannynguyen/opt/anaconda3/lib/python3.7/site-packages/sklearn/svm/base.py:929: ConvergenceWarning: Liblinear failed to converge, increase the number of iterations.
  "the number of iterations.", ConvergenceWarning)
[[2434  116]
 [ 345  165]]
TN: 165, FN: 116, TP: 2434, FP: 345
	Accuracy     : 0.85
	Precision    : 0.88
	Recall       : 0.95
	F1           : 0.91
	Informedness : 0.28
== End Classifier: LinearSVC
== -------------
== Start Classifier: SVC
[[2401  149]
 [ 401  109]]
TN: 109, FN: 149, TP: 2401, FP: 401
	Accuracy     : 0.82
	Precision    : 0.86
	Recall       : 0.94
	F1           : 0.90
	Informedness : 0.16
== End Classifier: SVC
== -------------